package fr.plethora.module;

import fr.plethora.model.Data;
import fr.plethora.snmp.SNMPManager;
import org.snmp4j.smi.OID;

public class Module implements IModule {

    OID oidPercentMemoryUsed  = new OID(".1.3.6.1.4.1.2021.9.1.9.1");

    private static String DISK_LOAD = "DISK_LOAD";

    public int getInterval() {
        return 60*1_000;
    }

    public Data getResult(SNMPManager manager) {
        try {
            String usedMemory = manager.getAsString(oidPercentMemoryUsed);
            Double disk = Double.valueOf(usedMemory);

            return new Data(DISK_LOAD, disk.toString());
        } catch (Exception e) {
            return new Data(DISK_LOAD, "NOT FOUND");
        }
    }

    public String getModuleName() {
        return DISK_LOAD;
    }
}
