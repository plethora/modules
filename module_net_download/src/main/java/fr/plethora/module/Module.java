package fr.plethora.module;

import fr.plethora.model.Data;
import fr.plethora.snmp.SNMPManager;

import java.util.ArrayList;
import java.util.List;

import org.snmp4j.smi.OID;

public class Module implements IModule {

    private boolean firstTime = true;
    private String oldTotalInOctets = new String();
    private long oldTime; 
    

    private static String NET_DOWNLOAD = "NET_DOWNLOAD";

    public int getInterval() {
        return 10*1_000;
    }

    public Data getResult(SNMPManager manager) {
        try {
            Double totalInOctets = new Double(0);
            Double downloadUse = new Double(0);
            
            OID oidInOctets  = new OID(".1.3.6.1.2.1.2.2.1.10");
            String oidInOctetsBegin  = ".1.3.6.1.2.1.2.2.1.10.";
            
            List<String> inOctetsList = new ArrayList<String>();
            
            long currentTime = System.currentTimeMillis();
            
            int oidCount = 1;
            int index = 0;
            
            String inOctets = "";
            
			while(inOctets  != "noSuchInstance" && inOctets  != "noSuchObject") {
				oidInOctets.setValue(oidInOctetsBegin+String.valueOf(oidCount));
            	inOctets = manager.getAsString(oidInOctets);
            	inOctetsList.add(inOctets);
            	oidCount++;
            }
			
			while(inOctetsList.get(index)  != "noSuchInstance" && inOctetsList.get(index)  != "noSuchObject") {
				totalInOctets += Double.valueOf(inOctetsList.get(index));
				index++;
			}
			
			
            
            if (this.firstTime) {
            	this.oldTotalInOctets = String.valueOf(totalInOctets);
            	this.oldTime = System.currentTimeMillis();
            	this.firstTime = false;
            	
            }else {
            	Double deltaOctets = Double.valueOf(totalInOctets) - Double.valueOf(this.oldTotalInOctets);
            	Double deltaTime = Double.valueOf(currentTime) - Double.valueOf(this.oldTime);
            	downloadUse = (  (deltaOctets*8) / (deltaTime/1000)  )/1024;
            }
            
            this.oldTotalInOctets = String.valueOf(totalInOctets);
            this.oldTime = currentTime;

            return new Data(NET_DOWNLOAD, downloadUse.toString());
        } catch (Exception e) {
            return new Data(NET_DOWNLOAD, "NOT FOUND");
        }
    }

    public String getModuleName() {
        return NET_DOWNLOAD;
    }
    
}
