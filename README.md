<div style="text-align: center">
    <img src="logo.png" alt="plethora logo">
    <p>a nagios-like application aiming at the IOT world!</p>
</div>

<hr>

# | Modules

Repository containing the modules that can be used by all the agents running.
Each module will collect a specific metric based on one or more SNMP key.


## Installation

Once the modules are compiled, you should drop them in the module folder of the agent of your device

## Contributors

+ [Constant Deschietere](mailto:constant.deschietere@cpe.fr)
+ [Rudy Deal](mailto:rudy.deal@cpe.fr)
+ [Maxime Collot](mailto:maxime.collot@cpe.fr)
+ [Florian Croisot](mailto:constant.deschietere@cpe.fr)
+ [Jordan Quagliatini](mailto:jordan.quagliatini@cpe.fr)
