package fr.plethora.module;

import fr.plethora.model.Data;
import fr.plethora.snmp.SNMPManager;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.snmp4j.smi.OID;

public class Module implements IModule {

    private static String CPU_CAPACITY = "CPU_HARDWARE";

    public int getInterval() {
        return 60*1_000;
    }

    public Data getResult(SNMPManager manager) {
        try {
            
        	Process p = Runtime.getRuntime().exec("bash -c lscpu");
            p.waitFor();
            BufferedReader reader =

                           new BufferedReader(new InputStreamReader(p.getInputStream()));

           StringBuilder sb = new StringBuilder();

           String line =  reader.readLine();

           while ((line = reader.readLine())!= null) {
               if(line.contains("Model name:")) {
                   sb.append(line + "\n");
               }
           }

           String infoCpu = sb.toString().substring(sb.toString().indexOf(':') + 1, sb.length()).trim();
           
            
            return new Data(CPU_CAPACITY, infoCpu);
        } catch (Exception e) {
            return new Data(CPU_CAPACITY, "NOT FOUND");
        }
    }

    public String getModuleName() {
        return CPU_CAPACITY;
    }
    
}
