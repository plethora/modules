package fr.plethora.module;

import fr.plethora.model.Data;
import fr.plethora.snmp.SNMPManager;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Module implements IModule {

    public int getInterval() {
        return 60*2*1_000;
    }

    public Data getResult(SNMPManager manager) {
        try {
            String url = "https://api.ipify.org/";
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            return new Data("IP_ADDRESS", response.toString());

        } catch (Exception e) {
            return new Data("IP_ADDRESS", "NOT FOUND");
        }
    }

    public String getModuleName() {
        //COMMON should change if the module has some system specific settings
        return "IP_" + "COMMON";
    }

    public static void main(String[] args) throws Exception {
        Module module = new Module();
        System.out.println(module.getResult(null).getValue());
    }
}
