package fr.plethora.module;

import fr.plethora.model.Data;
import fr.plethora.snmp.SNMPManager;

import java.util.ArrayList;
import java.util.List;

import org.snmp4j.smi.OID;

public class Module implements IModule {

    private boolean firstTime = true;
    private String oldTotalOutOctets = new String();
    private long oldTime; 
    

    private static String NET_UPLOAD = "NET_UPLOAD";

    public int getInterval() {
        return 10*1_000;
    }

    public Data getResult(SNMPManager manager) {
        try {
        	 Double totalOutOctets = new Double(0);
             Double uploadUse = new Double(0);
             
             OID oidOutOctets  = new OID(".1.3.6.1.2.1.2.2.1.16");
             String oidOutOctetsBegin  = ".1.3.6.1.2.1.2.2.1.16.";
             
             List<String> outOctetsList = new ArrayList<String>();
             
             long currentTime = System.currentTimeMillis();
             
             int oidCount = 1;
             int index = 0;
             
             String outOctets = "";
             
 			while(outOctets  != "noSuchInstance" && outOctets  != "noSuchObject") {
 				oidOutOctets.setValue(oidOutOctetsBegin+String.valueOf(oidCount));
 				outOctets = manager.getAsString(oidOutOctets);
             	outOctetsList.add(outOctets);
             	oidCount++;
             }
 			
 			while(outOctetsList.get(index)  != "noSuchInstance" && outOctetsList.get(index)  != "noSuchObject") {
 				totalOutOctets += Double.valueOf(outOctetsList.get(index));
 				index++;
 			}
 			
            
            
            if (this.firstTime) {
            	this.oldTotalOutOctets = String.valueOf(totalOutOctets);
            	this.oldTime = System.currentTimeMillis();
            	this.firstTime = false;
            	
            }else {
            	Double deltaOctets = Double.valueOf(totalOutOctets) - Double.valueOf(this.oldTotalOutOctets);
            	Double deltaTime = Double.valueOf(currentTime) - Double.valueOf(this.oldTime);
            	uploadUse = (  (deltaOctets*8) / (deltaTime/1000)  )/1024;
            }
            
            this.oldTotalOutOctets = String.valueOf(totalOutOctets);
            this.oldTime = currentTime;

            return new Data(NET_UPLOAD, uploadUse.toString());
        } catch (Exception e) {
            return new Data(NET_UPLOAD, "NOT FOUND");
        }
    }

    public String getModuleName() {
        return NET_UPLOAD;
    }
    
}
