package fr.plethora.module;

import fr.plethora.model.Data;
import fr.plethora.snmp.SNMPManager;
import org.snmp4j.smi.OID;

public class Module implements IModule {

    OID oid  = new OID(".1.3.6.1.4.1.2021.9.1.6.1");

    public int getInterval() {
        return 2*60*1_000;
    }

    public Data getResult(SNMPManager manager) {
        try {
            String disk = manager.getAsString(oid);
            return new Data("DISK_CAPACITY", disk);
        } catch (Exception e) {
            return new Data("DISK_CAPACITY", "NOT FOUND");
        }
    }

    public String getModuleName() {
        return "DISK_CAPACITY";
    }
}
