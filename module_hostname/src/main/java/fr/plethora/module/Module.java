package fr.plethora.module;

import fr.plethora.model.Data;
import fr.plethora.snmp.SNMPManager;
import org.snmp4j.smi.OID;

import java.io.IOException;

public class Module implements IModule {
    private OID NAME = new OID(".1.3.6.1.2.1.1.5.0");

    public int getInterval() {
        return 60*2*1_000;
    }

    public Data getResult(SNMPManager manager) {
        try {
            return new Data("HOSTNAME", manager.getAsString(NAME));
        } catch (IOException e) {
            //TODO LOG SMTG
        }
        return new Data("HOSTNAME", "NOT FOUND");
    }

    public String getModuleName() {
        //COMMON should change if the module has some system specific settings
        return "HOSTNAME_" + "COMMON";
    }
}
