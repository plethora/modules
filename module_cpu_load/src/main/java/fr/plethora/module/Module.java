package fr.plethora.module;

import fr.plethora.model.Data;
import fr.plethora.snmp.SNMPManager;
import org.snmp4j.smi.OID;

public class Module implements IModule {

    OID oidFreeCpu = new OID(".1.3.6.1.4.1.2021.11.11.0");

    private static String CPU_LOAD = "CPU_LOAD";

    public int getInterval() {
        return 10*1_000;
    }

    public Data getResult(SNMPManager manager) {
        try {
            String freeCPU = manager.getAsString(oidFreeCpu);
            Double cpu = 100 - Double.valueOf(freeCPU);

            return new Data(CPU_LOAD, cpu.toString());
        } catch (Exception e) {
            return new Data(CPU_LOAD, "NOT FOUND");
        }
    }

    public String getModuleName() {
        return CPU_LOAD;
    }
}
