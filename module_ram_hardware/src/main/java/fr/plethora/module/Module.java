package fr.plethora.module;

import fr.plethora.model.Data;
import fr.plethora.snmp.SNMPManager;
import org.snmp4j.smi.OID;

public class Module implements IModule {

    OID oid  = new OID(".1.3.6.1.4.1.2021.4.5.0");

    public int getInterval() {
        return 2*60*1_000;
    }

    public Data getResult(SNMPManager manager) {
        try {
            String ram = manager.getAsString(oid);
            return new Data("RAM_HARDWARE", ram);
        } catch (Exception e) {
            return new Data("RAM_HARDWARE", "NOT FOUND");
        }
    }

    public String getModuleName() {
        return "RAM_HARDWARE";
    }
}
