package fr.plethora.module;

import fr.plethora.model.Data;
import fr.plethora.snmp.SNMPManager;
import org.snmp4j.smi.OID;

public class Module implements IModule {

    OID oidTotalRam  = new OID(".1.3.6.1.4.1.2021.4.5.0");
    OID oidFreeRam = new OID(".1.3.6.1.4.1.2021.4.6.0");

    private static String RAM_LOAD = "RAM_LOAD";

    public int getInterval() {
        return 10*1_000;
    }

    public Data getResult(SNMPManager manager) {
        try {
            String totalRam = manager.getAsString(oidTotalRam);
            String freeMemory = manager.getAsString(oidFreeRam);
            Double ram = (Double.valueOf(totalRam) - Double.valueOf(freeMemory))/Double.valueOf(totalRam)*100;

            return new Data(RAM_LOAD, ram.toString());
        } catch (Exception e) {
            return new Data(RAM_LOAD, "NOT FOUND");
        }
    }

    public String getModuleName() {
        return RAM_LOAD;
    }
}
